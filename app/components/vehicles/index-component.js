import Component from '@ember/component';
import Ember from 'ember';
export default Component.extend({
  realtime: Ember.inject.service(),
  urls: Ember.inject.service(),
  store: Ember.inject.service(),
  customers: Ember.computed(function(){
    return this.get('store').findAll('customer');
  }),
  didInsertElement: function(){
    this.get('realtime').startConnection("vehicle_notifier");

  },
  actions: {
    selectCustomer(id){
      this.set('customerId', id);
    },
    selectStatus(status){
      this.set('status', status);
    }
  }
});
