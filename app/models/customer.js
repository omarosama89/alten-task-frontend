import DS from 'ember-data';

export default DS.Model.extend({
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  name: Ember.computed('firstName', 'lastName', function(){
    return `${this.firstName} ${this.lastName}`;
  }),
  address: DS.attr('string')
});
