import DS from 'ember-data';

export default DS.Model.extend({
  vehicleId: DS.attr('string'),
  regNum: DS.attr('string'),
  status: DS.attr('string'),
  customer: DS.belongsTo('customer')
});
