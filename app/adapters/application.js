import ENV from "../config/environment";
import Ember from 'ember';
import DS from 'ember-data';
import { pluralize } from 'ember-inflector';

export default DS.JSONAPIAdapter.extend({
  host: ENV.host,
  pathForType: function (type) {
    let a = pluralize(type);
    return Ember.String.underscore(a);
  }
});
