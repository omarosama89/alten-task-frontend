import Route from '@ember/routing/route';

export default Route.extend({
  realtime: Ember.inject.service(),

  beforeModel(){
    this.transitionTo('index.vehicles')
  },
  // afterModel(){
  //     this.get('realtime').startConnection("vehicle_notifier");
  // }
});
