import Route from '@ember/routing/route';

export default Route.extend({
  model(){
    return this.store.query('vehicle', {queryParams: {customer_id: this.get('customerId'), status: this.get('status')}});
  },
  actions: {
    refresh(...args){
      this.set('customerId', args[0]);
      this.set('status', args[1]);
      this.refresh();
    }
  }
});
