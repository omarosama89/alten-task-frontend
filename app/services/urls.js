import Ember from 'ember';
import ENV from '../config/environment';

export default Ember.Service.extend({
  customers: '/customers',
  vehicles: '/vehicles',

  getUrl: function (path) {
    return ENV.host + '/' + this.get('session').getRole() + this.get(path);
  },
  getAuxUrl: function(path, params){
    let str = '';
    if(params){
      for(let key in params){
        str += `${key}=${params[key]}&`
      }
    }
    return ENV.host + this.get(path) + '?' + str;
  },
  getWSHost: function(){
    return ENV.wsHost;
  }
});
