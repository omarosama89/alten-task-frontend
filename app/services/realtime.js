import Ember from 'ember';
import ENV from '../config/environment';
export default Ember.Service.extend({
  socketIOService: Ember.inject.service('socket-io'),
  connectionUrl: ENV.wsHost,
  store: Ember.inject.service(),

  startConnection(channel) {
    let socket = this.get('socketIOService').socketFor(this.get('connectionUrl'));
    socket.on(`${channel}`, this.onMessage, this);

  },
  onMessage(data) {
    this.get('store').pushPayload('vehicle', {vehicle: JSON.parse(data.vehicle)});
  },


});
